# Prerequisites

Download and install the following before using this software.

- [Node.js](https://nodejs.org/en/) - Needed for running both the backend and frontend
- [MongoDB](https://www.mongodb.com/try/download/community) - Needed for backend storage. Note, if this is your first time installing MongoDB on Windows, you may need to create a /data/db folder on the root of your C: drive in order for the service to properly start.


- [Angular CLI](https://cli.angular.io/) - Needed to build and run the frontend

# Backend

## Running a local server

Run the MongoDB service by running the `mongod.exe` file. This will be located where you installed MongoDB in the `MongoDB\Server\VERSION#\bin` folder.

Then, run `npm install` from the `/backend` directory to install necessary dependencies

Finally, start the backend service with `node index`

# Frontend

## Running a local server

Run `npm install` from the `/frontend` directory to install necessary dependencies

You will need to enter the IP address for your backend server in the  [frontend endpoint address file](frontend/src/app/endpoint-address.ts) in order for the frontend to properly make endpoint calls.

Start the frontend server with `ng serve --host 0.0.0.0`

This will allow access anywhere on your local network by using the server's local IP address at port 4200

Example: 192.168.1.X:4200
