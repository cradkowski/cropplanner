const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// set up express app
const app = express();

// connect to mongodb
mongoose.connect('mongodb://localhost/cropplanner');
mongoose.Promise = global.Promise;

app.use(express.json());

app.use(cors());


// initialize  routes
app.use('/api', require('./routes/api'));

// error handling middleware
app.use(function (err, req, res, next) {
    console.log(err);
    res.status(422).send({ error: err.message });

});


// listen for requests
app.listen(4000, function () {
    console.log('now listening for requests');
});
