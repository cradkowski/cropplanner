const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CropSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name field is required']
    },
    family: {
        type: String
    },
    daysToMaturity: {
        type: Number
    },
    isDirectSeeded: Boolean,
    weeksGrownPriorToTransplant: Number,
    seedGerminationTemperature: Number,
    seedInventory: Number,
    plantSpacing: Number
});

const Crop = mongoose.model('crop', CropSchema);

module.exports = Crop;