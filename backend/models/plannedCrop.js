const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CropSchema = require('../models/crop').schema;

const PlannedCropSchema = new Schema({
    crop: CropSchema,
    numberOfCropsPlanted: Number,
    amountSeededPerCrop: Number,
    isHydroponic: Boolean,
    plantingArea: String,
    isEnteredDateHarvestDate: Boolean,
    plantDate: Date,
    harvestDate: Date,
    seedDate: Date
});


const PlannedCrop = mongoose.model('plannedCrop', PlannedCropSchema);

module.exports = PlannedCrop;