const express = require('express');
const Crop = require('../models/crop');
const PlannedCrop = require('../models/plannedCrop');
const router = express.Router();

const FIRST_MONTH_STARTS_AT_ZERO_OFFSET = 1;
const NUMBER_OF_DAYS_IN_A_WEEK = 7;

// Date calculation functions:

function calculateHarvestDate(enteredDate, daysToMaturity, weeksGrownPriorToTransplant) {
    dates = {}
    var plantDate = new Date(enteredDate);

    var harvestDate = new Date(plantDate.getFullYear(), plantDate.getMonth(), plantDate.getDate());

    harvestDate.setDate(harvestDate.getDate() + daysToMaturity);

    var seedDate = new Date(plantDate.getFullYear(), plantDate.getMonth(), plantDate.getDate());

    seedDate.setDate(seedDate.getDate() - (weeksGrownPriorToTransplant * NUMBER_OF_DAYS_IN_A_WEEK));

    dates["harvestDate"] = harvestDate;
    dates["plantDate"] = plantDate;
    dates["seedDate"] = seedDate;

    return dates;
}

function calculatePlantDate(enteredDate, daysToMaturity, weeksGrownPriorToTransplant) {
    dates = {};
    var harvestDate = new Date(enteredDate);

    var plantDate = new Date(harvestDate.getFullYear(), harvestDate.getMonth(), harvestDate.getDate());

    plantDate.setDate(harvestDate.getDate() - daysToMaturity);

    var seedDate = new Date(plantDate.getFullYear(), plantDate.getMonth(), plantDate.getDate());

    seedDate.setDate(seedDate.getDate() - (weeksGrownPriorToTransplant * NUMBER_OF_DAYS_IN_A_WEEK));

    dates["harvestDate"] = harvestDate;
    dates["plantDate"] = plantDate;
    dates["seedDate"] = seedDate;

    return dates;
}



// Catalog API:

// get a list of crops from the database
router.get('/cropplanner/cropcatalog', function (req, res, next) {
    Crop.find({}).then(function (crops) {
        res.send(crops);
    });
});

router.get('/cropplanner/cropcatalog/:id', function (req, res, next) {
    Crop.findById(req.params.id).then(function (crop) {
        res.send(crop);
    });
});

router.get('/cropplanner/cropcatalog/name/:name', function (req, res, next) {
    Crop.find({ name: req.params.name }).then(function (crops) {
        res.send(crops);
    });
});

router.get('/cropplanner/cropcatalog/family/:family', function (req, res, next) {
    Crop.find({ family: req.params.family }).then(function (crops) {
        res.send(crops);
    });
});

// add a new crop to the database
router.post('/cropplanner/cropcatalog', function (req, res, next) {
    Crop.create(req.body).then(function (crop) {
        res.send(crop);
    }).catch(next);
});

// update a crop in the database
router.put('/cropplanner/cropcatalog/:id', function (req, res, next) {
    Crop.findByIdAndUpdate({ _id: req.params.id }, req.body).then(function () {
        Crop.findOne({ _id: req.params.id }).then(function (crop) {
            res.send(crop);
        });
    });
});

// add seeds to a crop
router.put('/cropplanner/cropcatalog/seeds/:id', function (req, res, next) {
    var seeds = 0;
    Crop.findById(req.params.id).then(function (crop) {
        seeds = parseInt(crop.seedInventory);
        seeds += parseInt(req.body.seedInventory);
    }).then(function () {
        Crop.findByIdAndUpdate(req.params.id, { seedInventory: parseInt(seeds) }).then(function () {
            Crop.findById(req.params.id).then(function (crop) {
                res.send(crop);
            });
        });
    });

});

// delete a crop from the database
router.delete('/cropplanner/cropcatalog/:id', function (req, res, next) {
    Crop.findByIdAndRemove({ _id: req.params.id }).then(function (crop) {
        res.send(crop);
    });
});

// PlannedCrops API:


router.get('/cropplanner/plannedcrops', function (req, res, next) {
    PlannedCrop.find({}).then(function (plannedCrops) {
        res.send(plannedCrops);
    });
});

router.get('/cropplanner/plannedcrops/plantingarea/:plantingarea', function (req, res, next) {
    PlannedCrop.find({ plantingArea: req.params.plantingarea }).then(function (plannedCrops) {
        res.send(plannedCrops);
    });
});

router.get('/cropplanner/plannedcrops/cropfamily/:cropfamily', function (req, res, next) {
    PlannedCrop.find({ 'crop.family': req.params.cropfamily }).then(function (plannedCrops) {
        res.send(plannedCrops);
    });
});

router.get('/cropplanner/plannedcrops/areacalculation/:plantingarea', function (req, res, next) {
    PlannedCrop.find({ plantingArea: req.params.plantingarea }).then(function (plannedCrops) {
        var areaUsage = 0.0;

        // Loop through each plannedCrop in a container (in actual use this would be all plannedCrops in the same row
        // retrieved from a DB search)
        for (var i = 0; i < plannedCrops.length; i++) {

            // If this planned crop group isn't the first one AND there is another crop before it 
            // with a larger spacing, use the larger crop's spacing when spacing the two types of crops
            if (i > 0 && plannedCrops[i - 1].crop.plantSpacing > plannedCrops[i].crop.plantSpacing) {
                // Add the larger crop's spacing for the first one since it has greater spacing
                // requirements
                areaUsage += plannedCrops[i - 1].crop.plantSpacing;
                // Use the current crop spacing for the rest of the row spacing
                areaUsage += (plannedCrops[i].numberOfCropsPlanted - 1) * plannedCrops[i].crop.plantSpacing;
            }

            // Else spacing is just number of crops * the current spacing requirement
            else {
                areaUsage += plannedCrops[i].numberOfCropsPlanted * plannedCrops[i].crop.plantSpacing;
            }

            // add extra space for last crop in the row so it isn't planted on the edge with no space
            if (i == (plannedCrops.length - 1)) {
                areaUsage += plannedCrops[i].crop.plantSpacing;
            }

        }
        res.send(areaUsage.toString());
    });
});


router.post('/cropplanner/plannedcrops', function (req, res, next) {
    var newPlannedCrop = req.body;
    var dates = {};
    if (req.body.isEnteredDateHarvestDate == "true") {
        dates = calculatePlantDate(req.body.enteredDate, req.body.crop.daysToMaturity, req.body.crop.weeksGrownPriorToTransplant);
    }

    else if (req.body.isEnteredDateHarvestDate == "false") {
        dates = calculateHarvestDate(req.body.enteredDate, req.body.crop.daysToMaturity, req.body.crop.weeksGrownPriorToTransplant);
    }

    delete newPlannedCrop.enteredDate;
    newPlannedCrop["harvestDate"] = dates.harvestDate;
    newPlannedCrop["plantDate"] = dates.plantDate;
    newPlannedCrop["seedDate"] = dates.seedDate;

    PlannedCrop.create(newPlannedCrop).then(function (plannedCrops) {
        res.send(plannedCrops);
    }).catch(next);
});

router.put('/cropplanner/plannedcrops/:id', function (req, res, next) {

    PlannedCrop.findById(req.params.id).then(function (plannedCrop) {

        var updatedPlanner = req.body;

        // if changing the flags at all, need to recalculate dates
        if (req.body.isEnteredDateHarvestDate == "true") {
            var dates = {};
            // if there's a new date, use this, otherwise use the stored date on the planner
            if (req.body.enteredDate) {
                dates = calculatePlantDate(req.body.enteredDate, plannedCrop.crop.daysToMaturity, plannedCrop.crop.weeksGrownPriorToTransplant);
            }
            else {
                dates = calculatePlantDate(plannedCrop.harvestDate, plannedCrop.crop.daysToMaturity, plannedCrop.crop.weeksGrownPriorToTransplant);
            }

            delete updatedPlanner.enteredDate;
            updatedPlanner["harvestDate"] = dates.harvestDate;
            updatedPlanner["plantDate"] = dates.plantDate;
            updatedPlanner["seedDate"] = dates.seedDate;
        }

        else if (req.body.isEnteredDateHarvestDate == "false") {
            var dates = {};
            // if there's a new date, use this, otherwise use the stored date on the planner
            if (req.body.enteredDate) {
                dates = calculateHarvestDate(req.body.enteredDate, plannedCrop.crop.daysToMaturity, plannedCrop.crop.weeksGrownPriorToTransplant);
            }
            else {
                dates = calculateHarvestDate(plannedCrop.plantDate, plannedCrop.crop.daysToMaturity, plannedCrop.crop.weeksGrownPriorToTransplant);
            }
            delete updatedPlanner.enteredDate;
            updatedPlanner["harvestDate"] = dates.harvestDate;
            updatedPlanner["plantDate"] = dates.plantDate;
            updatedPlanner["seedDate"] = dates.seedDate;
        }

        // otherwise, if there's a new date, recalculate using stored flag
        else if (req.body.enteredDate) {
            var dates = {};
            if (plannedCrop.isEnteredDateHarvestDate == true) {
                dates = calculatePlantDate(req.body.enteredDate, plannedCrop.crop.daysToMaturity, plannedCrop.crop.weeksGrownPriorToTransplant);
            }

            else {
                dates = calculateHarvestDate(req.body.enteredDate, plannedCrop.crop.daysToMaturity, plannedCrop.crop.weeksGrownPriorToTransplant);
            }
            delete updatedPlanner.enteredDate;
            updatedPlanner["harvestDate"] = dates.harvestDate;
            updatedPlanner["plantDate"] = dates.plantDate;
            updatedPlanner["seedDate"] = dates.seedDate;
        }
        PlannedCrop.findByIdAndUpdate(req.params.id, updatedPlanner).then(function () {
            PlannedCrop.findById(req.params.id).then(function (plannedCrop) {
                res.send(plannedCrop);
            });
        });

    });
});

router.delete('/cropplanner/plannedcrops/:id', function (req, res, next) {
    PlannedCrop.findByIdAndRemove({ _id: req.params.id }).then(function (plannedCrops) {
        res.send(plannedCrops);
    });
});

module.exports = router;

