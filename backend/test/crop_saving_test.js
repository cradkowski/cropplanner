const assert = require('assert');
const Crop = require('../models/crop');

describe('Saving Crop records', function () {

    it('Saves a Crop record to the database', function (done) {
        var crop = new Crop({
            name: 'Buttercrunch',
            family: 'Lettuce',
            daysToMaturity: 64,
            isDirectSeeded: true,
            weeksGrownPriorToTransplant: 0,
            seedGerminationTemperature: 60,
            seedInventory: 500,
            plantSpacing: 10
        });

        crop.save().then(function () {
            assert(crop.isNew === false);
            done();
        });

    });


})