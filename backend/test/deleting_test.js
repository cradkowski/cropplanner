const assert = require('assert');
const Crop = require('../models/crop');
const PlannedCrop = require('../models/plannedCrop');

describe('Deleting records', function () {
    var crop;
    var plannedCrop;

    beforeEach(function (done) {
        crop = new Crop({
            name: 'Ace',
            family: 'Pepper',
            daysToMaturity: 84,
            isDirectSeeded: false,
            weeksGrownPriorToTransplant: 8,
            seedGerminationTemperature: 80,
            seedInventory: 25,
            plantSpacing: 12
        });

        crop.save().then(function () {
            plannedCrop = new PlannedCrop({
                crop: crop,
                numberOfCropsPlanted: 4,
                amountSeededPerCrop: 3,
                isHydroponic: true,
                plantingArea: 'Row3',
                isEnteredDateHarvestDate: false,
                plantDate: new Date(2021, 4, 31),
                harvestDate: new Date(2021, 6, 31),
                seedDate: new Date(2021, 3, 31)
            });
            plannedCrop.save().then(function () {
                done();
            });
        });
    });

    it('Deletes Crop from the database', function (done) {
        Crop.findOneAndRemove({ name: 'Buttercrunch' }).then(function () {
            Crop.findOne({ name: 'Buttercrunch' }).then(function (result) {
                assert(result === null);
                done();
            });
        });
    });

    it('Deletes PlannedCrop from the database', function (done) {
        PlannedCrop.findOneAndRemove({ plantingArea: 'Row3' }).then(function () {
            PlannedCrop.findOne({ plantingArea: 'Row3' }).then(function (result) {
                assert(result === null);
                done();
            });
        });
    });

});