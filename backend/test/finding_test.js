const assert = require('assert');
const Crop = require('../models/crop');
const PlannedCrop = require('../models/plannedCrop');

// Describe tests
describe('Finding records', function () {
    // Create tests

    var crop;
    var plannedCrop;

    beforeEach(function (done) {
        crop = new Crop({
            name: 'Ace',
            family: 'Pepper',
            daysToMaturity: 84,
            isDirectSeeded: false,
            weeksGrownPriorToTransplant: 8,
            seedGerminationTemperature: 80,
            seedInventory: 25,
            plantSpacing: 12
        });

        crop.save().then(function () {
            plannedCrop = new PlannedCrop({
                crop: crop,
                numberOfCropsPlanted: 4,
                amountSeededPerCrop: 3,
                isHydroponic: true,
                plantingArea: 'Row3',
                isEnteredDateHarvestDate: false,
                plantDate: new Date(2021, 4, 31),
                harvestDate: new Date(2021, 6, 31),
                seedDate: new Date(2021, 3, 31)
            });
            plannedCrop.save().then(function () {
                done();
            });
        });
    });

    it('Finds one Crop record from the database', function (done) {
        Crop.findOne({ name: 'Ace' }).then(function (result) {
            assert(result.name === 'Ace');
            done();
        });

    });

    it('Finds one Crop record by ID from the database', function (done) {
        Crop.findOne({ _id: crop._id }).then(function (result) {
            assert(result._id.toString() === crop._id.toString());
            done();
        });

    });

    it('Finds one PlannedCrop record from the database', function (done) {
        PlannedCrop.findOne({ plantingArea: 'Row3' }).then(function (result) {
            assert(result.plantingArea === 'Row3');
            done();
        });

    });

    it('Finds one PlannedCrop record by ID from the database', function (done) {
        PlannedCrop.findOne({ _id: plannedCrop._id }).then(function (result) {
            assert(result._id.toString() === plannedCrop._id.toString());
            done();
        });

    });


})