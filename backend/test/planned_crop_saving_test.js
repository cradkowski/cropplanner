const assert = require('assert');
const mongoose = require('mongoose');
const PlannedCrop = require('../models/plannedCrop');
const Crop = require('../models/crop');

describe('Saving PlannedCrop records', function () {

    it('Creates a PlannedCrop with an associated Crop', function (done) {

        var firstPlannedCrop = new PlannedCrop({
            crop: new Crop ({
                name: 'Ace',
                family: 'Pepper',
                daysToMaturity: 84,
                isDirectSeeded: false,
                weeksGrownPriorToTransplant: 8,
                seedGerminationTemperature: 80,
                seedInventory: 25,
                plantSpacing: 12
            }),
            numberOfCropsPlanted: 4,
            amountSeededPerCrop: 3,
            isHydroponic: true,
            plantingArea: 'Row3',
            isEnteredDateHarvestDate: false,
            plantDate: new Date(2021, 4, 31),
            harvestDate: new Date(2021, 6, 31),
            seedDate: new Date(2021, 3, 31)

        });

        firstPlannedCrop.save().then(function () {
            PlannedCrop.findOne({ plantingArea: 'Row3' }).then(function (record) {
                assert(record.crop.family === 'Pepper');
                done();
            });
        });
    });



});