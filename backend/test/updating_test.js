const assert = require('assert');
const Crop = require('../models/crop');
const PlannedCrop = require('../models/plannedCrop');

describe('Updating records', function () {
    var crop;
    var plannedCrop;

    beforeEach(function (done) {
        crop = new Crop({
            name: 'Ace',
            family: 'Pepper',
            daysToMaturity: 84,
            isDirectSeeded: false,
            weeksGrownPriorToTransplant: 8,
            seedGerminationTemperature: 80,
            seedInventory: 25,
            plantSpacing: 12
        });

        crop.save().then(function () {
            plannedCrop = new PlannedCrop({
                crop: crop,
                numberOfCropsPlanted: 4,
                amountSeededPerCrop: 3,
                isHydroponic: true,
                plantingArea: 'Row3',
                isEnteredDateHarvestDate: false,
                plantDate: new Date(2021, 4, 31),
                harvestDate: new Date(2021, 6, 31),
                seedDate: new Date(2021, 3, 31)
            });
            plannedCrop.save().then(function () {
                done();
            });
        });
    });

    it('Updates one Crop record from the database', function (done) {
        Crop.findOneAndUpdate({ name: "Ace" }, { name: "Ember" }).then(function () {
            Crop.findOne({ _id: crop._id }).then(function (result) {
                assert(result.name === "Ember");
                done();
            });
        });

    });

    it('Updates one plannedCrop record from the database', function (done) {
        PlannedCrop.findOneAndUpdate({ plantingArea: "Row3" }, { numberOfCropsPlanted: 15 }).then(function () {
            PlannedCrop.findOne({ _id: plannedCrop._id }).then(function (result) {
                assert(result.numberOfCropsPlanted === 15);
                done();
            });
        });

    });

});