import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NewCropComponent } from './catalog-operations/new-crop/new-crop.component';
import { NewPlannerComponent } from './planner-operations/new-planner/new-planner.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import {MatFormFieldModule} from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';








import { ReactiveFormsModule } from '@angular/forms';
import { CatalogOperationsComponent } from './catalog-operations/catalog-operations.component';
import { PlannerOperationsComponent } from './planner-operations/planner-operations.component';
import { DisplayAllCropsComponent } from './catalog-operations/display-all-crops/display-all-crops.component';
import { SearchForCropComponent } from './catalog-operations/search-for-crop/search-for-crop.component';
import { ModifyCropComponent } from './catalog-operations/modify-crop/modify-crop.component';
import { AddSeedsComponent } from './catalog-operations/add-seeds/add-seeds.component';
import { DeleteCropComponent } from './catalog-operations/delete-crop/delete-crop.component';
import { CalculateAreaComponent } from './planner-operations/calculate-area/calculate-area.component';
import { DisplayAllPlannedCropsComponent } from './planner-operations/display-all-planned-crops/display-all-planned-crops.component';
import { SearchPlannedCropsComponent } from './planner-operations/search-planned-crops/search-planned-crops.component';
import { DeletePlannedCropComponent } from './planner-operations/delete-planned-crop/delete-planned-crop.component';

import { CatalogDataServiceService} from './catalog-data-service.service';
import { UpdatePlannedCropComponent } from './planner-operations/update-planned-crop/update-planned-crop.component';

@NgModule({
  declarations: [
    AppComponent,
    NewCropComponent,
    NewPlannerComponent,
    CatalogOperationsComponent,
    PlannerOperationsComponent,
    DisplayAllCropsComponent,
    SearchForCropComponent,
    ModifyCropComponent,
    AddSeedsComponent,
    DeleteCropComponent,
    CalculateAreaComponent,
    DisplayAllPlannedCropsComponent,
    SearchPlannedCropsComponent,
    DeletePlannedCropComponent,
    UpdatePlannedCropComponent
    
   
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule, BrowserAnimationsModule, MatSliderModule,
    MatExpansionModule, MatFormFieldModule, MatButtonModule, MatRadioModule, MatInputModule, ReactiveFormsModule, MatTableModule, MatSortModule,
    MatToolbarModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule
   
  ],
  providers: [CatalogDataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
