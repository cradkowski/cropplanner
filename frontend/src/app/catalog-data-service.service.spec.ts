import { TestBed } from '@angular/core/testing';

import { CatalogDataServiceService } from './catalog-data-service.service';

describe('CatalogDataServiceService', () => {
  let service: CatalogDataServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatalogDataServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
