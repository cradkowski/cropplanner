import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EndpointAddress } from './endpoint-address';
import { from } from 'rxjs';
import { Crop } from './crop';



@Injectable({
  providedIn: 'root'
})
export class CatalogDataServiceService {

  constructor(private http: HttpClient) { }

  getAllCatalogData() {
    return from(this.http.get<Crop[]>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog')  )

  }


}
