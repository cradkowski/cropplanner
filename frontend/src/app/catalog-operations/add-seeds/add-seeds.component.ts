import { Component, OnInit } from '@angular/core';
import { EndpointAddress } from 'src/app/endpoint-address';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-add-seeds',
  templateUrl: './add-seeds.component.html',
  styleUrls: ['./add-seeds.component.css']
})
export class AddSeedsComponent implements OnInit {
  seedCropGroup: FormGroup;
  numStatus;

  error: string = null;
  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.seedCropGroup = this.formBuilder.group({
      _id: [, [Validators.required]],
      seedAmount: [, [Validators.required]]
    })
  }


  updateSeeds(): void {

    this.numStatus = null;

    this.http.put(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog/seeds/' + this.seedCropGroup.controls._id.value, {
      seedInventory: this.seedCropGroup.controls.seedAmount.value}).subscribe(data => console.log(data));
  }
}
