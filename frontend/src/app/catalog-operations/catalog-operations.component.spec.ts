import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CatalogOperationsComponent } from './catalog-operations.component';

describe('CatalogOperationsComponent', () => {
  let component: CatalogOperationsComponent;
  let fixture: ComponentFixture<CatalogOperationsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogOperationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
