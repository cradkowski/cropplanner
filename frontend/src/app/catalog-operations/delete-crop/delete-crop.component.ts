import { Component, OnInit } from '@angular/core';
import { EndpointAddress } from 'src/app/endpoint-address';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-delete-crop',
  templateUrl: './delete-crop.component.html',
  styleUrls: ['./delete-crop.component.css']
})
export class DeleteCropComponent implements OnInit {
  deleteCropGroup: FormGroup;
  numStatus;
  error;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.deleteCropGroup = this.formBuilder.group({
      cropNumber: [, [Validators.required]]
    })
  }

  deleteCropByNumber(): void {
    this.numStatus = null;
    this.error = null;
    this.http.delete<number>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog/' + this.deleteCropGroup.controls.cropNumber.value)
      .subscribe(data => {
        this.numStatus = data;
      },
        (error: HttpErrorResponse) => {

          this.error = error.error;

        }
      );
  }

}
