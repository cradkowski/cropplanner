import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DisplayAllCropsComponent } from './display-all-crops.component';

describe('DisplayAllNumberOrderComponent', () => {
  let component: DisplayAllCropsComponent;
  let fixture: ComponentFixture<DisplayAllCropsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayAllCropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayAllCropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
