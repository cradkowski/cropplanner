import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CatalogDataServiceService } from '../../catalog-data-service.service';


@Component({
  selector: 'app-display-all-crops',
  templateUrl: './display-all-crops.component.html',
  styleUrls: ['./display-all-crops.component.css'],
  providers: [CatalogDataServiceService]
})


export class DisplayAllCropsComponent implements OnInit{
  dataSource;
  displayedColumns: string[] = ['_id', 'name', 'family', 'daysToMaturity', 'isDirectSeeded', 'weeksGrownPriorToTransplant', 'seedGerminationTemperature', 'seedInventory', 'plantSpacing'];

  constructor(private catalogDataService: CatalogDataServiceService) { }

  ngOnInit() {
    this.catalogDataService.getAllCatalogData().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
    }); 
  }

  @ViewChild(MatSort) sort: MatSort;
}
