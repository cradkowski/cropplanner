import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModifyCropComponent } from './modify-crop.component';

describe('ModifyCropComponent', () => {
  let component: ModifyCropComponent;
  let fixture: ComponentFixture<ModifyCropComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyCropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
