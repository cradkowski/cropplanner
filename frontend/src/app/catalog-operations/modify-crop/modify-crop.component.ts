import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { EndpointAddress } from '../../endpoint-address';

import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-modify-crop',
  templateUrl: './modify-crop.component.html',
  styleUrls: ['./modify-crop.component.css']
})
export class ModifyCropComponent implements OnInit {

  modifyCropGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.modifyCropGroup = this.formBuilder.group({
      _id: [, [Validators.required]],
      name: [, [Validators.required]],
      family: [, [Validators.required]],
      daysToMaturity: [, [Validators.required]],
      isDirectSeeded: [, [Validators.required]],
      weeksGrownPriorToTransplant: [, [Validators.required]],
      seedGerminationTemperature: [, [Validators.required]],
      seedInventory: [, [Validators.required]],
      plantSpacing: [, [Validators.required]]
    })
  }

  modifyCrop() {

    var updatedCrop = {};

    if (this.modifyCropGroup.controls.name.value) {
      updatedCrop["name"] = this.modifyCropGroup.controls.name.value;
    }

    if (this.modifyCropGroup.controls.family.value) {
      updatedCrop["family"] = this.modifyCropGroup.controls.family.value;
    }

    if (this.modifyCropGroup.controls.daysToMaturity.value) {
      updatedCrop["daysToMaturity"] = this.modifyCropGroup.controls.daysToMaturity.value;
    }

    if (this.modifyCropGroup.controls.isDirectSeeded.value) {
      updatedCrop["isDirectSeeded"] = this.modifyCropGroup.controls.isDirectSeeded.value;
    }

    if (this.modifyCropGroup.controls.weeksGrownPriorToTransplant.value) {
      updatedCrop["weeksGrownPriorToTransplant"] = this.modifyCropGroup.controls.weeksGrownPriorToTransplant.value;
    }

    if (this.modifyCropGroup.controls.seedGerminationTemperature.value) {
      updatedCrop["seedGerminationTemperature"] = this.modifyCropGroup.controls.seedGerminationTemperature.value;
    }

    if (this.modifyCropGroup.controls.seedInventory.value) {
      updatedCrop["seedInventory"] = this.modifyCropGroup.controls.seedInventory.value;
    }

    if (this.modifyCropGroup.controls.plantSpacing.value) {
      updatedCrop["plantSpacing"] = this.modifyCropGroup.controls.plantSpacing.value;
    }

    console.log(updatedCrop);

    this.http.put(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog/' + this.modifyCropGroup.controls._id.value, updatedCrop).subscribe(data => console.log(data));
    //this.http.put(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog/' + this.modifyCropGroup.controls.id.value, updatedCrop.subscribe(data => console.log(data));
  }

}
