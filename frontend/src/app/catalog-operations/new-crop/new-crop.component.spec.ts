import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewCropComponent } from './new-crop.component';

describe('NewCropComponent', () => {
  let component: NewCropComponent;
  let fixture: ComponentFixture<NewCropComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
