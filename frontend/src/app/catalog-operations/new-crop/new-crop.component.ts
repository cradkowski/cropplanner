import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { EndpointAddress } from '../../endpoint-address';

import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-new-crop',
  templateUrl: './new-crop.component.html',
  styleUrls: ['./new-crop.component.css']
})
export class NewCropComponent implements OnInit {
  newCropGroup: FormGroup;

  cropnumreturned: number = null;
  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.newCropGroup = this.formBuilder.group({
      name: [, [Validators.required]],
      family: [, [Validators.required]],
      daysToMaturity: [, [Validators.required]],
      isDirectSeeded: [, [Validators.required]],
      weeksGrownPriorToTransplant: [, [Validators.required]],
      seedGerminationTemperature: [, [Validators.required]],
      seedInventory: [, [Validators.required]],
      plantSpacing: [, [Validators.required]]
    })
  }

  createCrop() {
    this.http.post(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog', {
      name: this.newCropGroup.controls.name.value,
      family: this.newCropGroup.controls.family.value,
      daysToMaturity: this.newCropGroup.controls.daysToMaturity.value,
      isDirectSeeded: this.newCropGroup.controls.isDirectSeeded.value,
      weeksGrownPriorToTransplant: this.newCropGroup.controls.weeksGrownPriorToTransplant.value,
      seedGerminationTemperature: this.newCropGroup.controls.seedGerminationTemperature.value,
      seedInventory: this.newCropGroup.controls.seedInventory.value,
      plantSpacing: this.newCropGroup.controls.plantSpacing.value
    }).subscribe(data => console.log(data));
  }
}
