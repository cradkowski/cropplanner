import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SearchForCropComponent } from './search-for-crop.component';

describe('SearchForCropComponent', () => {
  let component: SearchForCropComponent;
  let fixture: ComponentFixture<SearchForCropComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchForCropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchForCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
