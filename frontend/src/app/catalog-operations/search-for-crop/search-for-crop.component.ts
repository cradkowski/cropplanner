import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Crop } from 'src/app/crop';
import { EndpointAddress } from 'src/app/endpoint-address';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-search-for-crop',
  templateUrl: './search-for-crop.component.html',
  styleUrls: ['./search-for-crop.component.css']
})
export class SearchForCropComponent implements OnInit {
  searchCropGroup: FormGroup;
  dataSource;
  displayedColumns: string[] = ['_id', 'name', 'family', 'daysToMaturity', 'isDirectSeeded', 'weeksGrownPriorToTransplant', 'seedGerminationTemperature', 'seedInventory', 'plantSpacing'];
  crops: Array<Crop> = null;
  error: string = null;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.searchCropGroup = this.formBuilder.group({
      searchTerm: [, [Validators.required]],
      searchFilter: [, [Validators.required]]
    })
  }


  searchForCrops(): void {
    this.error = null;
    this.crops = null;

    if (this.searchCropGroup.controls.searchFilter.value == "cropName") {

      this.http.get<Array<Crop>>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog/name/' + this.searchCropGroup.controls.searchTerm.value)
        .subscribe((data) => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        },
          (error: HttpErrorResponse) => {

            this.error = error.error;

          }
        );

    }

    else {
      this.http.get<Array<Crop>>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog/family/' + this.searchCropGroup.controls.searchTerm.value)
        .subscribe((data) => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        },
          (error: HttpErrorResponse) => {

            this.error = error.error;

          }
        );
    }
  }

  @ViewChild(MatSort) sort: MatSort;
}