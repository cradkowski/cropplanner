export class Crop {
    id: string;
    name: string;
    family: string;
    daysToMaturity: number;
    isDirectSeeded: boolean;
    weeksGrownPriorToTransplant: number;
    seedGerminationTemperature: number;
    seedInventory: number;
    plantSpacing: number;
}
