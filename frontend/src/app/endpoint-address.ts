export class EndpointAddress {
    // See: https://www.w3schools.com/js/js_classes.asp


    static ipAddress: string = 'localhost';
    static port: number = 4000;

    static getEndpointAddress() {
        return 'http://' + EndpointAddress.ipAddress + ':' + EndpointAddress.port;
    }

    static setIPAddress(ipAddress) {
        EndpointAddress.ipAddress = ipAddress;
    }

    static setPort(port) {
        EndpointAddress.port = port;
    }
    
}

