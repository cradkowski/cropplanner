import { TestBed } from '@angular/core/testing';

import { PlannerDataServiceService } from './planner-data-service.service';

describe('PlannerDataServiceService', () => {
  let service: PlannerDataServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlannerDataServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
