import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EndpointAddress } from './endpoint-address';
import { from } from 'rxjs';
import { Planner } from './planner';

@Injectable({
  providedIn: 'root'
})
export class PlannerDataServiceService {

  constructor(private http: HttpClient) { }

  getAllPlannerData() {
    return from(this.http.get<Planner[]>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/plannedcrops')  )

  }
}
