import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { EndpointAddress } from 'src/app/endpoint-address';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'


@Component({
  selector: 'app-calculate-area',
  templateUrl: './calculate-area.component.html',
  styleUrls: ['./calculate-area.component.css']
})
export class CalculateAreaComponent implements OnInit {
  calculateAreaGroup: FormGroup;
  numStatus: number;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.calculateAreaGroup = this.formBuilder.group({
      searchArea: [, [Validators.required]]
    })
  }

  calculateArea() {
    this.http.get<number>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/plannedcrops/areacalculation/' + this.calculateAreaGroup.controls.searchArea.value).subscribe((data) => {
      this.numStatus = data;
    });
  }

}
