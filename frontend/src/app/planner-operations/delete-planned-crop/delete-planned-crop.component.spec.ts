import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeletePlannedCropComponent } from './delete-planned-crop.component';

describe('DeletePlannedCropComponent', () => {
  let component: DeletePlannedCropComponent;
  let fixture: ComponentFixture<DeletePlannedCropComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletePlannedCropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePlannedCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
