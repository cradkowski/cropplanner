import { Component, OnInit } from '@angular/core';
import { EndpointAddress } from 'src/app/endpoint-address';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-delete-planned-crop',
  templateUrl: './delete-planned-crop.component.html',
  styleUrls: ['./delete-planned-crop.component.css']
})
export class DeletePlannedCropComponent implements OnInit {
  deletePlannerGroup: FormGroup;
  numStatus;
  error;
  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.deletePlannerGroup = this.formBuilder.group({
      plannerNumber: [, [Validators.required]]
    })
  }

  deletePlannerByNumber(): void {
    this.numStatus = null;
    this.error = null;
    this.http.delete<number>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/plannedcrops/' + this.deletePlannerGroup.controls.plannerNumber.value)
      .subscribe(data => {
        this.numStatus = data;
      },
        (error: HttpErrorResponse) => {

          this.error = error.error;

        }
      );
  }

}
