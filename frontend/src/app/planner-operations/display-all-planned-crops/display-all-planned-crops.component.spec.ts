import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DisplayAllPlannedCropsComponent } from './display-all-planned-crops.component';

describe('DisplayAllPlannedCropsComponent', () => {
  let component: DisplayAllPlannedCropsComponent;
  let fixture: ComponentFixture<DisplayAllPlannedCropsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayAllPlannedCropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayAllPlannedCropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
