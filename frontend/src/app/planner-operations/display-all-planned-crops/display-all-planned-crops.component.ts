import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PlannerDataServiceService } from '../../planner-data-service.service';

@Component({
  selector: 'app-display-all-planned-crops',
  templateUrl: './display-all-planned-crops.component.html',
  styleUrls: ['./display-all-planned-crops.component.css']
})
export class DisplayAllPlannedCropsComponent implements OnInit {
  dataSource;
  displayedColumns: string[] = ['_id', 'crop-name', 'crop-family', 'numberOfCropsPlanted', 'amountSeededPerCrop', 'isHydroponic', 'plantingArea', 'seedDate', 'plantDate', 'harvestDate'];

  constructor(private plannerDataService: PlannerDataServiceService) { }

  ngOnInit() {
    this.plannerDataService.getAllPlannerData().subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
    });
  }
  @ViewChild(MatSort) sort: MatSort;
}
