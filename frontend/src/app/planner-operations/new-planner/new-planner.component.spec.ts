import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewPlannerComponent } from './new-planner.component';

describe('NewPlannerComponent', () => {
  let component: NewPlannerComponent;
  let fixture: ComponentFixture<NewPlannerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPlannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPlannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
