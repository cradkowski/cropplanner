import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { EndpointAddress } from '../../endpoint-address';

import { Crop } from '../../crop';

import { FormGroup, FormBuilder, Validators } from '@angular/forms'


@Component({
  selector: 'app-new-planner',
  templateUrl: './new-planner.component.html',
  styleUrls: ['./new-planner.component.css']
})
export class NewPlannerComponent implements OnInit {
  error: string = null;
  newPlannerGroup: FormGroup;
  plannernumreturned: number = null;
  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.newPlannerGroup = this.formBuilder.group({
      cropNumber: [, [Validators.required]],
      numberOfCropsPlanted: [, [Validators.required]],
      amountSeededPerCrop: [, [Validators.required]],
      isHydroponic: [, [Validators.required]],
      isEnteredDateHarvestDate: [, [Validators.required]],
      plantingArea: [, [Validators.required]],
      seeds: [, [Validators.required]],
      enteredDate: [, [Validators.required]]
    })
  }

  createPlanner() {

    this.http.get<Crop>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/cropcatalog/' + this.newPlannerGroup.controls.cropNumber.value)
      .subscribe((data) => {
        console.log(data);
        this.http.post(EndpointAddress.getEndpointAddress() + '/api/cropplanner/plannedcrops',
          {
            crop: data,
            numberOfCropsPlanted: this.newPlannerGroup.controls.numberOfCropsPlanted.value,
            amountSeededPerCrop: this.newPlannerGroup.controls.amountSeededPerCrop.value,
            isHydroponic: this.newPlannerGroup.controls.isHydroponic.value,
            isEnteredDateHarvestDate: this.newPlannerGroup.controls.isEnteredDateHarvestDate.value,
            plantingArea: this.newPlannerGroup.controls.plantingArea.value,
            enteredDate: this.newPlannerGroup.controls.enteredDate.value
          })
          .subscribe((data: number) => {
            this.plannernumreturned = data;
          }
          );
      },
        (error: HttpErrorResponse) => {

        }
      );
  }
}
