import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlannerOperationsComponent } from './planner-operations.component';

describe('PlannerOperationsComponent', () => {
  let component: PlannerOperationsComponent;
  let fixture: ComponentFixture<PlannerOperationsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlannerOperationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannerOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
