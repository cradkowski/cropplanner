import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SearchPlannedCropsComponent } from './search-planned-crops.component';

describe('SearchPlannedCropsComponent', () => {
  let component: SearchPlannedCropsComponent;
  let fixture: ComponentFixture<SearchPlannedCropsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPlannedCropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPlannedCropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
