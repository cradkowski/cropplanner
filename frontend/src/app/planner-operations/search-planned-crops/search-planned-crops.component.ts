import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Planner } from 'src/app/planner';
import { EndpointAddress } from 'src/app/endpoint-address';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-search-planned-crops',
  templateUrl: './search-planned-crops.component.html',
  styleUrls: ['./search-planned-crops.component.css']
})
export class SearchPlannedCropsComponent implements OnInit {
  searchPlannerGroup: FormGroup;
  dataSource;
  displayedColumns: string[] = ['_id', 'crop-name', 'crop-family', 'numberOfCropsPlanted', 'amountSeededPerCrop', 'isHydroponic', 'plantingArea', 'seedDate', 'plantDate', 'harvestDate'];
  planners: Array<Planner> = null;
  error: string = null;


  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.searchPlannerGroup = this.formBuilder.group({
      searchTerm: [, [Validators.required]],
      searchFilter: [, [Validators.required]]
    })
  }
  searchForPlanners(): void {
    this.error = null;
    this.planners = null;

    if (this.searchPlannerGroup.controls.searchFilter.value == "plannerArea") {

      this.http.get<Array<Planner>>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/plannedcrops/plantingarea/' + this.searchPlannerGroup.controls.searchTerm.value)
        .subscribe((data) => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        },
          (error: HttpErrorResponse) => {

            this.error = error.error;

          }
        );

    }

    else {
      this.http.get<Array<Planner>>(EndpointAddress.getEndpointAddress() + '/api/cropplanner/plannedcrops/cropfamily/' + this.searchPlannerGroup.controls.searchTerm.value)
        .subscribe((data) => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        },
          (error: HttpErrorResponse) => {

            this.error = error.error;

          }
        );
    }
  }


  @ViewChild(MatSort) sort: MatSort;
}
