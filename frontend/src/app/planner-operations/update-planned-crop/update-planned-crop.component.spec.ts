import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePlannedCropComponent } from './update-planned-crop.component';

describe('UpdatePlannedCropComponent', () => {
  let component: UpdatePlannedCropComponent;
  let fixture: ComponentFixture<UpdatePlannedCropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatePlannedCropComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePlannedCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
