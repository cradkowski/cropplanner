import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { EndpointAddress } from '../../endpoint-address';

import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-update-planned-crop',
  templateUrl: './update-planned-crop.component.html',
  styleUrls: ['./update-planned-crop.component.css']
})
export class UpdatePlannedCropComponent implements OnInit {

  modifyPlannerGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.modifyPlannerGroup = this.formBuilder.group({
      _id: [, [Validators.required]],
      numberOfCropsPlanted: [, [Validators.required]],
      amountSeededPerCrop: [, [Validators.required]],
      isHydroponic: [, [Validators.required]],
      isEnteredDateHarvestDate: [, [Validators.required]],
      plantingArea: [, [Validators.required]],
      enteredDate: [, [Validators.required]]
    })
  }

  updatePlannedCrop() {

    var updatedPlanner = {};

    if (this.modifyPlannerGroup.controls.numberOfCropsPlanted.value) {
      updatedPlanner["numberOfCropsPlanted"] = this.modifyPlannerGroup.controls.numberOfCropsPlanted.value;
    }

    if (this.modifyPlannerGroup.controls.amountSeededPerCrop.value) {
      updatedPlanner["amountSeededPerCrop"] = this.modifyPlannerGroup.controls.amountSeededPerCrop.value;
    }

    if (this.modifyPlannerGroup.controls.isHydroponic.value) {
      updatedPlanner["isHydroponic"] = this.modifyPlannerGroup.controls.isHydroponic.value;
    }

    if (this.modifyPlannerGroup.controls.isEnteredDateHarvestDate.value) {
      updatedPlanner["isEnteredDateHarvestDate"] = this.modifyPlannerGroup.controls.isEnteredDateHarvestDate.value;
    }

    if (this.modifyPlannerGroup.controls.plantingArea.value) {
      updatedPlanner["plantingArea"] = this.modifyPlannerGroup.controls.plantingArea.value;
    }

    if (this.modifyPlannerGroup.controls.enteredDate.value) {
      updatedPlanner["enteredDate"] = this.modifyPlannerGroup.controls.enteredDate.value;
    }

    console.log(updatedPlanner);

    this.http.put(EndpointAddress.getEndpointAddress() + '/api/cropplanner/plannedcrops/' + this.modifyPlannerGroup.controls._id.value, updatedPlanner).subscribe(data => console.log(data));
  }

}
