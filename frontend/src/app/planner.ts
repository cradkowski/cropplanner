import { Crop } from './crop';

export class Planner {
    id: string;
    crop: Crop;  
    numberOfCropsPlanted: number;
    amountSeededPerCrop: number; 
    isHydroponic: boolean;
    plantingArea: string;
    isEnteredDateHarvestDate: boolean;
    plantDate: Date;
    harvestDate: Date;
    seedDate: Date;
}